import { Controller, Get, Response, StreamableFile } from '@nestjs/common';
import { createReadStream } from 'fs';
import { join } from 'path/posix';

@Controller('file')
export class FileController {
  @Get()
  getFile(@Response({ passthrough: true }) res): StreamableFile {
    const file = createReadStream(join(process.cwd(), 'package.json'));
    res.headers('Content-Type', 'application/json');
    res.headers('Content-Disposition', 'attachment; filename="package.json"');

    return new StreamableFile(file);
  }
}
