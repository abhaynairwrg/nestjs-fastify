import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { Note, NoteDocument } from './models/note.model';
import { Model, ObjectId } from 'mongoose';

@Injectable()
export class NotesService {
  constructor(@InjectModel(Note.name) private noteModel: Model<NoteDocument>) {}

  async create(createNoteDto: CreateNoteDto) {
    try {
      const newNote = new this.noteModel(createNoteDto);

      return await newNote.save();
    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<Note[]> {
    try {
      return await this.noteModel.find();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: ObjectId): Promise<Note> {
    try {
      const note = await this.noteModel.findById(id);

      if (!note) {
        throw new NotFoundException('Resource not found');
      }

      return note;
    } catch (error) {
      throw error;
    }
  }

  async update(id: ObjectId, updateNoteDto: UpdateNoteDto) {
    try {
      await this.findOne(id);

      return await this.noteModel.findByIdAndUpdate(id, updateNoteDto);
    } catch (error) {
      throw error;
    }
  }

  async remove(id: ObjectId) {
    try {
      await this.findOne(id);

      return await this.noteModel.findByIdAndRemove(id);
    } catch (error) {
      throw error;
    }
  }
}
