import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UnprocessableEntityException,
} from '@nestjs/common';
import { NotesService } from './notes.service';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { isValidObjectId, ObjectId } from 'mongoose';

@Controller('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @Post()
  async create(@Body() createNoteDto: CreateNoteDto) {
    try {
      return await this.notesService.create(createNoteDto);
    } catch (error) {
      throw error;
    }
  }

  @Get()
  async findAll() {
    try {
      return await this.notesService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @Get(':id')
  async findOne(@Param('id') id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new UnprocessableEntityException('Bad object id');
      }

      return await this.notesService.findOne(id);
    } catch (error) {
      throw error;
    }
  }

  @Patch(':id')
  async update(
    @Param('id') id: ObjectId,
    @Body() updateNoteDto: UpdateNoteDto,
  ) {
    try {
      if (!isValidObjectId(id)) {
        throw new UnprocessableEntityException('Bad object id');
      }

      // If whole request body is empty, throw error
      if (JSON.stringify(updateNoteDto) === '{}') {
        throw new UnprocessableEntityException(
          'Please provide any field values to update',
        );
      }

      return await this.notesService.update(id, updateNoteDto);
    } catch (error) {
      throw error;
    }
  }

  @Delete(':id')
  async remove(@Param('id') id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new UnprocessableEntityException('Bad object id');
      }

      return await this.notesService.remove(id);
    } catch (error) {
      throw error;
    }
  }
}
